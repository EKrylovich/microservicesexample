#!/usr/bin/env bash

function note() {
    local GREEN NC
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    printf "\n${GREEN}$@  ${NC}\n" >&2
}

set -e


cd ../microservices/functional/budget-service;           note "Dependencies generation budget...";          ./gradlew dependencies --configuration compile; cd -
cd ../microservices/functional/cost-service;             note "Dependencies generation cost...";            ./gradlew dependencies --configuration compile; cd -
cd ../microservices/functional/scenario-service;         note "Dependencies generation scenario...";        ./gradlew dependencies --configuration compile; cd -
cd ../microservices/composite/composite-service;         note "Dependencies generation composite...";       ./gradlew dependencies --configuration compile; cd -
cd ../microservices/api/budget-api-service;              note "Dependencies generation api...";             ./gradlew dependencies --configuration compile; cd -

cd ../microservices/support/auth-server;                 note "Dependencies generation auth...";            ./gradlew dependencies --configuration compile; cd -
cd ../microservices/support/discovery-server;            note "Dependencies generation discovery...";       ./gradlew dependencies --configuration compile; cd -
cd ../microservices/support/edge-server;                 note "Dependencies generation edge...";            ./gradlew dependencies --configuration compile; cd -
cd ../microservices/support/monitor-dashboard;           note "Dependencies generation monitor...";         ./gradlew dependencies --configuration compile; cd -
cd ../microservices/support/turbine;                     note "Dependencies generation turbine...";         ./gradlew dependencies --configuration compile; cd -



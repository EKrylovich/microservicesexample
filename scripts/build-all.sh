#!/usr/bin/env bash

function note() {
    local GREEN NC
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    printf "\n${GREEN}$@  ${NC}\n" >&2
}
set -e

cd ../microservices/functional/budget-service;           note "Building budget...";          ./gradlew clean build; cd -
cd ../microservices/functional/cost-service;             note "Building cost...";            ./gradlew clean build; cd -
cd ../microservices/functional/scenario-service;         note "Building scenario...";        ./gradlew clean build; cd -
cd ../microservices/composite/composite-service;         note "Building composite...";       ./gradlew clean build; cd -
cd ../microservices/api/budget-api-service;              note "Building api...";             ./gradlew clean build; cd -

cd ../microservices/support/auth-server;                 note "Building auth...";            ./gradlew clean build; cd -
cd ../microservices/support/discovery-server;            note "Building discovery...";       ./gradlew clean build; cd -
cd ../microservices/support/edge-server;                 note "Building edge...";            ./gradlew clean build; cd -
cd ../microservices/support/monitor-dashboard;           note "Building monitor...";         ./gradlew clean build; cd -
cd ../microservices/support/turbine;                     note "Building turbine...";         ./gradlew clean build; cd -

find . -name *SNAPSHOT.jar -exec du -h {} \;

docker-compose build
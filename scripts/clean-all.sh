#!/usr/bin/env bash

function note() {
    local GREEN NC
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    printf "\n${GREEN}$@  ${NC}\n" >&2
}
set -e


cd ../microservices/functional/budget-service;           note "Cleaning budget...";          ./gradlew clean; cd -
cd ../microservices/functional/cost-service;             note "Cleaning cost...";            ./gradlew clean; cd -
cd ../microservices/functional/scenario-service;         note "Cleaning scenario...";        ./gradlew clean; cd -
cd ../microservices/composite/composite-service;         note "Cleaning composite...";       ./gradlew clean; cd -
cd ../microservices/api/budget-api-service;              note "Cleaning api...";             ./gradlew clean; cd -

cd ../microservices/support/auth-server;                 note "Cleaning auth...";            ./gradlew clean; cd -
cd ../microservices/support/discovery-server;            note "Cleaning discovery...";       ./gradlew clean; cd -
cd ../microservices/support/edge-server;                 note "Cleaning edge...";            ./gradlew clean; cd -
cd ../microservices/support/monitor-dashboard;           note "Cleaning monitor...";         ./gradlew clean; cd -
cd ../microservices/support/turbine;                     note "Cleaning turbine...";         ./gradlew clean; cd -
package com.evkryl.cost.model;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
public class Cost {
	private int costId;
	private int budgetId;
	private double price;
	private String entity;

	public Cost() {
	}

	public Cost(int costId, int budgetId, String entity, double price) {
		this.costId = costId;
		this.entity = entity;
		this.price = price;
		this.budgetId = budgetId;
	}

	public int getCostId() {
		return costId;
	}

	public void setCostId(int costId) {
		this.costId = costId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public int getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(int budgetId) {
		this.budgetId = budgetId;
	}
}

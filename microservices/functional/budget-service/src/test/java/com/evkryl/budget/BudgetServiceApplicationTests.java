package com.evkryl.budget;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.*;

/**
 * Created by ekrylovich
 * on 19.5.17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment= RANDOM_PORT)
@Ignore
// Instruct embedded Tomcat to run on a random free port and skip talking to the Config, Bus and Discovery server
//@IntegrationTest({"server.port=0", "spring.cloud.config.enabled=false", "spring.cloud.bus.enabled=false", "spring.cloud.discovery.enabled=false"})
public class BudgetServiceApplicationTests {
	@Test
	public void contextLoads() {
	}
}

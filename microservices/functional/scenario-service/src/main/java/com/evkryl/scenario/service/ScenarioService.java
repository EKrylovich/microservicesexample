package com.evkryl.scenario.service;

import com.evkryl.scenario.model.Scenario;
import com.evkryl.scenario.service.util.SetProcTimeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class ScenarioService {

	private static final Logger LOG = LoggerFactory.getLogger(ScenarioService.class);


	private final SetProcTimeBean setProcTimeBean;

	@Autowired
	public ScenarioService(final SetProcTimeBean setProcTimeBean) {
		this.setProcTimeBean = setProcTimeBean;
	}


	/**
	 * Sample usage: curl $HOST:$PORT/scenario?budgetId=1
	 *
	 * @param budgetId
	 * @return
	 */
	@RequestMapping("/scenario")
	public List<Scenario> getReviews(
			@RequestParam(value = "budgetId",  required = true) int budgetId) {

		int pt = setProcTimeBean.calculateProcessingTime();
		LOG.info("/reviews called, processing time: {}", pt);

		sleep(pt);


		List<Scenario> list = new ArrayList<>();
		list.add(new Scenario(1, budgetId, "Superman","Mega subject", "Super scenario"));
		list.add(new Scenario(2, budgetId, "Superman","Mega subject", "Super scenario"));
		list.add(new Scenario(3, budgetId, "Superman","Mega subject", "Super scenario"));

		LOG.info("/reviews response size: {}", list.size());

		return list;
	}

	private void sleep(int pt) {
		try {
			Thread.sleep(pt);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sample usage:
	 *
	 *  curl "http://localhost:10002/set-processing-time?minMs=1000&maxMs=2000"
	 *
	 * @param minMs
	 * @param maxMs
	 */
	@RequestMapping("/set-processing-time")
	public void setProcessingTime(
			@RequestParam(value = "minMs", required = true) int minMs,
			@RequestParam(value = "maxMs", required = true) int maxMs) {

		LOG.info("/set-processing-time called: {} - {} ms", minMs, maxMs);

		setProcTimeBean.setDefaultProcessingTime(minMs, maxMs);
	}
}

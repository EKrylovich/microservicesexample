package com.evkryl.scenario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by ekrylovich
 * on 6.5.17.
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ScenarioApplication {
	public static void main(String[] args) {
		SpringApplication.run(ScenarioApplication.class, args);
	}
}

package com.evkryl.composite.model.external;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
public class Budget {
	private int budgetId;
	private double amount;
	private String owner;
	private String goal;

	public Budget() {
	}

	public Budget(int budgetId, double amount, String owner, String goal) {
		this.budgetId = budgetId;
		this.amount = amount;
		this.owner = owner;
		this.goal = goal;
	}

	public int getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(int budgetId) {
		this.budgetId = budgetId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}
}

package com.evkryl.composite.model;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
public class CostSummary {
	private int costId;
	private double price;
	private String entity;

	public CostSummary(int costId, double price, String entity) {
		this.costId = costId;
		this.price = price;
		this.entity = entity;
	}

	public int getCostId() {
		return costId;
	}

	public double getPrice() {
		return price;
	}

	public String getEntity() {
		return entity;
	}
}

package com.evkryl.composite.model.external;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
public class Scenario {
	private int scenarioId;
	private int budgetId;
	private String author;
	private String subject;
	private String scenario;

	public Scenario() {
	}

	public Scenario(int scenarioId, int budgetId, String author, String subject, String scenario) {
		this.scenarioId = scenarioId;
		this.budgetId = budgetId;
		this.author = author;
		this.subject = subject;
		this.scenario = scenario;
	}

	public int getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}

	public int getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(int budgetId) {
		this.budgetId = budgetId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
}

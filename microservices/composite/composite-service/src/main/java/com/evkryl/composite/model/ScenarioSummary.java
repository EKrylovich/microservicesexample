package com.evkryl.composite.model;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
public class ScenarioSummary {
	private int scenarioId;
	private String author;
	private String scenario;

	public ScenarioSummary(int scenarioId, String author, String scenario) {
		this.scenarioId = scenarioId;
		this.author = author;
		this.scenario = scenario;
	}

	public int getScenarioId() {
		return scenarioId;
	}

	public String getAuthor() {
		return author;
	}

	public String getScenario() {
		return scenario;
	}
}

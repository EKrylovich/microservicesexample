package com.evkryl.api.budget.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestOperations;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.security.Principal;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


/**
 * Created by ekrylovich
 * on 17.5.17.
 */
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class BudgetApiService {

    private static final Logger LOG = LoggerFactory.getLogger(BudgetApiService.class);

    private final RestOperations restTemplate;

    @Autowired
    public BudgetApiService(final RestOperations restTemplate) {
        this.restTemplate = restTemplate;
    }

    @RequestMapping("/{budgetId}")
    @HystrixCommand(fallbackMethod = "defaultBudgetComposite")
    public ResponseEntity<String> getBudgetComposite(
        @PathVariable int budgetId,
        @RequestHeader(value="Authorization") String authorizationHeader,
        Principal currentUser) {

        LOG.info("BudgetApi: User={}, Auth={}, called with budgetId={}", currentUser.getName(), authorizationHeader, budgetId);
        String url = "http://composite-service/budget/" + budgetId;
        LOG.debug("GetBudgetComposite from URL: {}", url);

        ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
        LOG.info("GetBudgetComposite http-status: {}", result.getStatusCode());
        LOG.debug("GetBudgetComposite body: {}", result.getBody());

        return result;
    }

    /**
     * Fallback method for getBudgetComposite()
     *
     * @param budgetId
     * @return
     */
    public ResponseEntity<String> defaultBudgetComposite(
        @PathVariable int budgetId,
        @RequestHeader(value="Authorization") String authorizationHeader,
        Principal currentUser) {

        LOG.warn("Using fallback method for composite-service. User={}, Auth={}, called with budgetId={}", currentUser.getName(), authorizationHeader, budgetId);
        return new ResponseEntity<>("Empty response", HttpStatus.OK);
    }
}
